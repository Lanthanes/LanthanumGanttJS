"use strict";

/** @type {Function[]} */
export let yesNoCallback;

/**
 * 
 * @param {ModalType} type 
 * @param {String} message 
 * @param {Function} yesCallback 
 * @param {Function} noCallback 
 * @returns {Function[]}
 */
 export const OpenYesNoModal = (type, message, yesCallback, noCallback) => {
    
    OpenMessageModal("#YesNoModal", message);

    switch (type) {
        case ModalType.YesNo:
            break;        
        case ModalType.Yes:
            break;
    }

    yesNoCallback = [yesCallback, noCallback];
}

export const ModalType = {
    YesNo:  0,
    Yes:    1,
}
