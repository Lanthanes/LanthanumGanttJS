"use strict";

import * as date from '../modules/Date.js';
import * as utils from '../modules/Utils.js';
import * as GanttClass from '../modules/Class.js';
import * as view from '../modules/View.js';
import { main as config } from '../config.js';

export const SetWidthForTask = (d, e) => (date.DiffDay(d, e) * config.details.day_size_pixel) + "px";

/**
 * 
 * @param {GanttClass.Ticket} ticket 
 * @param {Date} dateStart 
 * @param {Date} dateEnd 
 * @param {Boolean} update (default: false)
 * @param {Element[]} content (content: [0: 'user list content', 1: 'graph list content']) (default: null)
 */
export const WriteTicketNode = (ticket, dateStart, dateEnd, update = false, content = null) => {

    if (!ticket.isDelete) {
    
        const tasks = utils.GanttData.data.tasks.filter(t => t.ticket == ticket.id && !t.isDelete);
    
        const minMax = utils.GetMinMaxDate(false, tasks);
    
        const data = [
            view.RenderTicket({ 
                ticket: ticket, 
            }),
            view.RenderTicketGraph({ 
                ticket: ticket, 
                width: SetWidthForTask(dateStart, dateEnd),
                dateStartTicket: minMax[0],
                dateEndTicket: minMax[1],
                ticketWith: SetWidthForTask(minMax[0], minMax[1]), 
                ticketMargin: SetWidthForTask(dateStart, minMax[0]), 
            })
        ];
    
        if (content !== null && content.length > 0) {
    
            // console.log("add ticket-0", ticket.id, content[0], utils.WriteNodeToString(data[0]));
            // console.log("add ticket-1", ticket.id, content[1], utils.WriteNodeToString(data[1]));
    
            content[0].appendChild(data[0]);
            content[1].appendChild(data[1]);
    
            tasks.filter(t => !t.isDelete).sort(t => t.order).reverse().forEach(/** @type {GanttClass.Task} */ task => WriteTaskNode(task, dateStart, dateEnd, false, [document.querySelector(`#tic${ticket.id}`), document.querySelector(`#tig${ticket.id}`)]));
        } else {
    
            let ticketInfo = document.querySelector(`#tic${ticket.id}`);
            let ticketGraph = document.querySelector(`#tig${ticket.id}`);
    
            if (ticketInfo != null && ticketGraph != null) {
    
                // console.log("edit ticket-0", ticket.id, ticketInfo, data[0]);
                // console.log("edit ticket-1", ticket.id, ticketGraph, ticketGraph.parentElement, data[1]);
        
                if (ticket.isDelete || tasks.length == 0 || tasks.filter(t => t.isDelete).length == tasks.length) {
    
                    ticketInfo.remove();
                    ticketGraph.parentElement.remove();
    
                    update = true;
                } else {
                    ticketInfo.outerHTML = utils.WriteNodeToString(data[0]);
                    ticketGraph.parentElement.outerHTML = utils.WriteNodeToString(data[1]);
    
                    if (update) tasks.filter(t => !t.isDelete).sort(t => t.order).reverse().forEach(/** @type {GanttClass.Task} */ task => WriteTaskNode(task, dateStart, dateEnd, false));
                }
            }
        }
        
        // if (update) WriteTicketNode(ticket, dateStart, dateEnd);
    }
}

/**
 * 
 * @param {GanttClass.Task} task 
 * @param {Date} dateStart 
 * @param {Date} dateEnd 
 * @param {Boolean} update (default: false)
 * @param {Element[]} after (content: [0: tas, 1: tag]) (default: null)
 */
 export const WriteTaskNode = (task, dateStart, dateEnd, update = false, after = null) => {

    if (!task.isDelete) {
    
        const ticket = utils.GetTicketOfTask(task);
    
        const data = [
            view.RenderTask({ 
                task: task, 
                ticket: ticket, 
            }), 
            view.RenderTaskGraph({
                task: task,
                ticket: ticket, 
                width: SetWidthForTask(dateStart, dateEnd),
                taskWith: SetWidthForTask(new Date(task.start), new Date(task.end)), 
                taskMargin: SetWidthForTask(dateStart, new Date(task.start)), 
            }), 
        ];
    
        if (after !== null && after.length > 0) {
    
            // console.log("add task-0", task.id, after[0], data[0]);
            // console.log("add task-1", task.id, after[1], after[1].parentElement, data[1]);
    
            after[0].after(data[0]);
            after[1].parentElement.after(data[1]);
        } else {
    
            let taskInfo = document.querySelector(`#tas${task.id}`);
            let taskGraph = document.querySelector(`#tag${task.id}`);
    
            if (taskInfo != null && taskGraph != null) {
    
                // console.log("edit task-0", task.id, taskInfo, data[0]);
                // console.log("edit task-1", task.id, taskGraph, taskGraph.parentElement, data[1]);
        
                if (task.isDelete || ticket.isDelete) {
    
                    taskInfo.remove();
                    taskGraph.parentElement.remove();
    
                    update = true;
                } else {
    
                    taskInfo.outerHTML = utils.WriteNodeToString(data[0]);
                    taskGraph.parentElement.outerHTML = utils.WriteNodeToString(data[1]);
                }
            }
        }
    
        // if (update) WriteTicketNode(ticket, dateStart, dateEnd, true);
    }
}

/**
 * 
 * @param {String} taskID 
 * @param {String} ticketID 
 * @param {Element} elmnt 
 * @param {Date} dateStart 
 * @param {Date} dateEnd 
 * @returns {GanttClass.Gantt}
 */
export const EditMoveGanttBar = (taskID, ticketID, elmnt, dateStart, dateEnd) => {

    const taskIndex = utils.GanttData.data.tasks.findIndex(t => t.id == taskID && t.ticket == ticketID);

    let newDateStart = new Date(dateStart);
    newDateStart.setDate(newDateStart.getDate() + (utils.StyleSizeToNumber(elmnt.style.marginLeft, "px") / config.details.day_size_pixel) - 1);

    let newDateEnd = new Date(newDateStart);
    newDateEnd.setDate(newDateEnd.getDate() + (utils.StyleSizeToNumber(elmnt.style.width, "px") / config.details.day_size_pixel) - 1);

    utils.GanttData.data.tasks[taskIndex].start = newDateStart.toISOString().split('T')[0];
    utils.GanttData.data.tasks[taskIndex].end = newDateEnd.toISOString().split('T')[0];
    
    WriteTicketNode(utils.GetTicketWithId(ticketID), dateStart, dateEnd, true);
}
