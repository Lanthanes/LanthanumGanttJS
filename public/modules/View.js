"use strict";

import * as utils from '../modules/Utils.js';
import * as edit from '../modules/Edit.js';
import * as date from '../modules/Date.js';
import { main as config } from '../config.js';

export const RenderTicket = object => {

    const render = `
        <div class="start-anim go-to-graph" id="tic${object.ticket.id}" style="opacity: 1;">
            <span class="left">
                <img src="${utils.CheckPicture(object.ticket.picture)}">
                <span>${object.ticket.label}</span>
            </span>
            <span class="right">#${object.ticket.id}</span>
        </div>
    `;

    return utils.WriteNode(render);
}

export const RenderTask = object => {

    const render = `
        <div class="start-anim user-content go-to-graph" id="tas${object.task.id}" data-ticketid="${object.ticket.id}" data-taskid="${object.task.id}" style="opacity: 1;">
            <span class="left">
                <img src="${utils.CheckPicture(utils.GanttData.data.profiles.find(d => d.id == object.task.profile).picture)}">
                <span>${utils.GanttData.data.profiles.find(d => d.id == object.task.profile).label}</span>
            </span>
            <span class="right">
                <a id="tas${object.task.id}" class="open-task" data-ticketid="${object.ticket.id}" data-taskid="${object.task.id}">
                    <i class="bi bi-pen-fill"></i>
                </a>
            </span>
        </div>
    `;

    return utils.WriteNode(render);
}

export const RenderTicketGraph = object => {

    const render = `
        <div class="pbar-delimitation" style="width: ${object.width};">
            <div id="tig${object.ticket.id}" class="progress pbar-ticket-background move-task-graph task-group" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="right" data-bs-html="true" data-ticketid="${object.ticket.id}" data-bs-original-title="<i class=&quot;bi bi-pc-display-horizontal&quot;></i> #${object.ticket.id} · ${object.ticket.label}" data-bs-content="<i class=&quot;bi bi-terminal&quot;></i> ${utils.GanttData.data.categories.find(c => c.id == object.ticket.category).label}<br><i class='bi bi-calendar'></i> <b>${new Date(object.dateStartTicket).FormatDateToString()}</b> · <b>${new Date(object.dateEndTicket).FormatDateToString()}</b><br><i class=&quot;bi bi-calendar-range&quot;></i> ${date.DiffDay(new Date(object.dateStartTicket), new Date(object.dateEndTicket))} <span class=&quot;trad-day_info&quot;>${config.language[config.format.language_code].ui.gantt.day_info}</span><br><i class=&quot;bi bi-hammer&quot;></i> ${"???"}%" style="width: ${object.ticketWith}; margin-left: ${object.ticketMargin}; opacity: 1;">
                <div class="progress-bar pbar-complete" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
            </div>
        </div>
    `;

    return utils.WriteNode(render);
}

export const RenderTaskGraph = object => {

    const render = `
        <div class="pbar-delimitation" style="width: ${object.width};">
            <div id="tag${object.task.id}" class="progress pbar-task-background selection-task move-task-graph" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="right" data-bs-html="true" data-ticketid="${object.ticket.id}" data-taskid="${object.task.id}" data-bs-original-title="<i class=&quot;bi bi-list-task&quot;></i> #${object.ticket.id} · ${object.task.label}" data-bs-content="<i class=&quot;bi bi-terminal&quot;></i> ${utils.GanttData.data.categories.find(c => c.id == object.ticket.category).label}<br><i class='bi bi-person-square'></i> ${utils.GanttData.data.profiles.find(d => d.id == object.task.profile).label}<br><i class=&quot;bi bi-pc-display-horizontal&quot;></i> #${object.ticket.id} · ${object.ticket.label}<br><i class='bi bi-calendar'></i> <b>${new Date(object.task.start).FormatDateToString()}</b> · <b>${new Date(object.task.end).FormatDateToString()}</b><br><i class=&quot;bi bi-calendar-range&quot;></i> ${date.DiffDay(new Date(object.task.start), new Date(object.task.end))} <span class=&quot;trad-day_info&quot;>${config.language[config.format.language_code].ui.gantt.day_info}</span><br><i class=&quot;bi bi-hammer&quot;></i> ${object.task.finished}%" style="width: ${object.taskWith}; margin-left: ${object.taskMargin}; opacity: 1;">
                <span class="progress-edit-bar left" data-edit-bar="left"></span>
                <div class="progress-bar pbar-complete" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ${object.task.finished}%;">${config.details.display_pourcent_on_progress_bar ? `${task.finished}%` : ""}</div>
                <span class="progress-edit-bar right" data-edit-bar="right"></span>
                <span class="label-task">#${object.ticket.id} · ${object.task.label}</span>
            </div>
        </div>
    `;

    return utils.WriteNode(render);
}

export const RenderWeekData = object => {

    const render = `
        <article>
            <header>
                <span>${config.language[config.format.language_code].ui.gantt.week_info} ${object.day.GetWeekOnYear()}</span>
                <span>${object.day.FormatDateToString()} | ${object.nextDay.FormatDateToString()}</span>
            </header>
            <div>${object.listOfDay}</div>
            <span class="bottom-shadow"></span>
        </article>
    `;

    return utils.WriteNode(render);
}

export const RenderDayInWeekData = object => {

    const render = `
        <span data-bs-toggle="tooltip" data-bs-placement="bottom" class="${object.day.setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0) ? 'now' : ''} ${object.dayOnWeek >= 5 ? ' weend' : ''}" data-bs-original-title="${config.language[config.format.language_code].ui.gantt.week_code[object.dayOnWeek]} ${utils.AddZeroToOneNumber(object.day.getDate())}">
            <span>${config.language[config.format.language_code].ui.gantt.week_code[object.dayOnWeek].slice(0, 3)}. ${utils.AddZeroToOneNumber(object.day.getDate())}</span>
        </span>
    `;

    return utils.WriteNode(render);
}

export const RenderWeekBackground = object => utils.WriteNode(`<article class="${object.status}">${object.listOfDay}</article>`);

export const RenderDayInWeekBackground = object => utils.WriteNode(`<div class="${object.day.setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0) ? 'now' : ''} ${object.dayOnWeek >= 5 ? 'weend' : ''}"></div>`);

export const RenderChecklistElement = object => {

    const render = `
        <div class="input-group mt-3 content-checklist">
            <div class="input-group-text">
                <input class="form-check-input mt-0" name="checklist-check" id="${object.id}" type="checkbox" ${object.checked != undefined && object.checked != null && object.checked ? 'checked' : ''}>
            </div>
            <input class="form-control" name="checklist-label" id="${object.id}" type="text" placeholder="${config.language[config.format.language_code].ui.gantt.enter_text}"${object.value != undefined && object.value != null ? ` value="${object.value}"` : ''}>
            <label class="input-group-text for-pointer" for="${object.id}"><i class="bi bi-check-lg"></i></label>
        </div>`;

    return utils.WriteNode(render);
}

export const RenderDependanceElement = object => {

    const render = `
        <div class="input-group mt-3 content-dependency">
            <div class="input-group-text">
                <input class="form-check-input mt-0" name="dependency-check" id="${object.task.id}" type="checkbox" ${object.checked != undefined && object.checked != null && object.checked ? 'checked' : ''}>
            </div>
            <label class="form-control" for="${object.task.id}">#${object.task.ticket} · ${utils.GetTicketWithId(object.task.ticket).label} <i class="bi bi-arrow-right"></i> ${utils.GanttData.data.profiles.find(i => i.id == object.task.profile).label}</label>
            <button id="${object.task.id}" class="open-edit-task-depandance" data-ticketid="${object.task.ticket}" data-taskid="${object.task.id}"><i class="bi bi-pencil-fill"></i></button>
        </div>`;

    return utils.WriteNode(render);
}

export const RenderOptionTag = object => utils.WriteNode(`<option value="${object.value}" ${object.selected != undefined && object.selected != null && object.selected ? 'selected' : ''} ${object.disabled != undefined && object.disabled != null && object.disabled ? 'disabled' : ''}>${object.name}</option>`);

export const RenderNoDataInput = _ => utils.WriteNode(`<div class="input-group"><span class="form-control">${config.language[config.format.language_code].ui.gantt.no_dependance}</span></div>`);

export const RenderManageList = object => {

    const render = `
        <div class="input-group mb-3">
            <label class="form-control" for="${object.target}-${object.id}">${object.name}</label>
            <button id="${object.target}-${object.id}" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal"><i class="bi bi-binoculars-fill"></i></button>
        </div>`;

    return utils.WriteNode(render);
}
