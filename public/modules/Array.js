"use strict"

Array.prototype.Last = function() {

    return this[this.length - 1];
};

Array.prototype.First = function() {

    return this[0];
};

Array.prototype.Clear = function() {
    
    this.length = 0;
    return this;
};
