"use strict";

import * as utils from './Utils.js';
import * as view from './View.js';

export const OpenModalTask = (ticketID, taskID) => {

    if (ticketID == null && taskID == null) {
        ticketID = -1;
        taskID = -1;
    }

    let myTicket = utils.GetTicketWithId(ticketID);
    let myTask = utils.GetTaskWithId(taskID);
    
    const ticketsSelect = document.querySelector('#TaskModal #tickets');
    const usersSelect = document.querySelector('#TaskModal #users');
    const rolesSelect = document.querySelector('#TaskModal #roles');
    const dependenciesSelect = document.querySelector('#TaskModal #list-dependencies');
    const checklistsSelect = document.querySelector('#TaskModal #list-checklists');
    
    ticketsSelect.innerHTML = "";
    usersSelect.innerHTML = "";
    rolesSelect.innerHTML = "";
    dependenciesSelect.innerHTML = "";
    checklistsSelect.innerHTML = "";

    document.querySelector('#TaskModal #TicketID').value = ticketID;
    document.querySelector('#TaskModal #TaskID').value = taskID;

    if (myTask !== undefined) {
        utils.ScrollTo(`#tag${taskID}`);

        document.querySelector('#TaskModal #label').value = myTask.label;
        document.querySelector('#TaskModal #description').value = myTask.description;
        document.querySelector('#TaskModal #finished').value = myTask.finished;
        document.querySelector('#TaskModal #task-date-start').value = new Date(myTask.start).toISOString().substring(0, 10);
        document.querySelector('#TaskModal #task-date-end').value = new Date(myTask.end).toISOString().substring(0, 10);
    } else {
        document.querySelector('#TaskModal #label').value = "";
        document.querySelector('#TaskModal #description').value = "";
        document.querySelector('#TaskModal #finished').value = 0;
        document.querySelector('#TaskModal #task-date-start').value = utils.GanttData.start.toISOString().substring(0, 10);
        document.querySelector('#TaskModal #task-date-end').value = utils.GanttData.end.toISOString().substring(0, 10);
    }

    /** TICKETS **/

    utils.GanttData.data.tickets.forEach(e => {
        ticketsSelect.appendChild(view.RenderOptionTag({
            value: e.id,
            selected: (myTicket !== undefined && myTicket.id == e.id),
            name: `#${e.id} · ${e.label}`,
        }));
    });

    /** USERS **/

    utils.GanttData.data.profiles.forEach(e => {
        usersSelect.appendChild(view.RenderOptionTag({
            value: e.id,
            selected: (myTask !== undefined && myTask.profile == e.id),
            name: e.label,
        }));
    });

    /** ROLES **/

    utils.GanttData.data.roles.forEach(e => {
        rolesSelect.appendChild(view.RenderOptionTag({
            value: e.id,
            selected: (myTask !== undefined && myTask.role == e.id),
            name: e.label,
        }));
    });

    /** DEPENDENCIES **/
    if (utils.GanttData.data.tasks.length > 0) {
        utils.GanttData.data.tasks.forEach(task => {
    
            if (taskID != task.id && !task.isDelete && !utils.GetTicketWithId(task.ticket).isDelete) {
                dependenciesSelect.appendChild(view.RenderDependanceElement({
                    task: task,
                    checked: (myTask !== undefined && myTask.dependencies.includes(task.id) ? 'checked' : ''),
                }));
            }

        });
    } else {
        dependenciesSelect.appendChild(view.RenderNoDataInput());
    }

    document.querySelectorAll('.open-edit-task-depandance').forEach(e => {
        e.addEventListener("click", a => {                
            OpenModalTask(a.currentTarget.getAttribute("data-TicketID"), a.currentTarget.getAttribute("data-TaskID"));
        });
    });
    
    /** CHECKLISTS **/

    if (myTask !== undefined && myTask.checklists.length > 0) {
        myTask.checklists.forEach(e => {
            if (!e.isDelete) {
                checklistsSelect.appendChild(view.RenderChecklistElement({
                    id: e.id,
                    checked: e.isFinished,
                    value: e.label,
                }));
            }
        });
    }

    checklistsSelect.appendChild(view.RenderChecklistElement({ id: utils.GenerateId() }));
    
    if (!$('#TaskModal').hasClass('show')) {
        $('#TaskModal').modal('toggle');
    }
}

export const OpenMessageModal = (modal, message) => {
    
    $(`${modal} .message`).html(message);
    $(`${modal}`).modal('show'); // .show();
}

/**
 * 
 * @param {String} type 
 */
export const OpenModalList = (type) => {

    const listContent = document.querySelector('#ListModal #list');
    listContent.innerHTML = "";

    document.querySelector('#ListModal .trad-list_title').innerHTML = "Liste ???"; // TODO to trad

    switch (type) {
        case "profiles":
            utils.GanttData.data.profiles.forEach(profile => {

                listContent.appendChild(view.RenderManageList({
                    id: profile.id, 
                    target: 'profile', 
                    name: profile.label, 
                }));
            });
            break;
        case "roles":
            utils.GanttData.data.roles.forEach(role => {

                listContent.appendChild(view.RenderManageList({
                    id: role.id, 
                    target: 'role', 
                    name: role.label, 
                }));
            });
            break;
        case "categories":
            utils.GanttData.data.categories.forEach(category => {

                listContent.appendChild(view.RenderManageList({
                    id: category.id, 
                    target: 'category', 
                    name: category.label, 
                }));
            });
            break;
    }
    
    $('#ListModal').modal('toggle');
}

export const OpenModalEdit = (type) => {

    const profiles_inputs = [""];
    const roles_inputs = [""];
    const categories_inputs = [""];

    console.log(type);
}
