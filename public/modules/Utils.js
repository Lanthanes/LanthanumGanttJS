"use strict";

import { main as config } from '../config.js';
import * as GanttClass from '../modules/Class.js';
import * as array from '../modules/Array.js';
import * as modal from '../modules/Modal.js';
import * as date from '../modules/Date.js';
import * as view from '../modules/View.js';

const gsapDataOptions = { duration: 0.5, delay: 3, stagger: 0.1, opacity: 1, ease: Elastic.easeOut.config(1, 0.3) };
const gsapGraphOptions = { duration: 1, delay: 3, stagger: 0.1, opacity: 0, width: 0, ease: "power2.out" };
const gsapLineOptions = { hide: true, gradient: true, startPlugColor: '#3498db', endPlugColor: '#2980b9', startPlug: 'disc', endPlug: 'disc', dropShadow: { color: '#bdc3c7', dx: 0, dy: 0 } }

let savePromise = null;
let historic = new Array();
let indexHistoric = 0;
let index = 0;
let ganttToSend;


/** @type {GanttClass.Gantt} */
export let GanttData;

/**
 * 
 * @param {GanttClass.Gantt} gantt 
 * @returns {GanttClass.Gantt} 
 */

export const SetGanttData = gantt => GanttData = gantt;

export const SelectionType = {
    SIZE:   0,
    PLACE:  1,
}

export const InsertCodePosition = {
    BEFOREBEGIN:    'beforebegin',
    AFTERBEGIN:     'afterbegin',
    BEFOREEND:      'beforeend',
    AFTEREND:       'afterend',
}

export const GenerateId = (size = 16) => {
    return Math.random().toString(size).slice(2);
}

/**
 * 
 * @param {String} link 
 * @param {String} data 
 * @param {String} dataType     default => html
 * @param {String} type         default => GET
 */
export const CallAjaxURL = (link, dataType, type, data) => {

    if (dataType == null) dataType = 'html';
    if (type == null) type = 'GET';

    return $.ajax({
        url : link,
        dataType : (dataType == null ?  'html' : dataType),
        type : (type == null ?  'GET' : type),
        data : data,
        crossDomain: true,
        jsonpCallback: 'processJSONPResponse',
    });
};

export const AnimateOnLoad = async () => {

    gsap.fromTo([".start-anim"], { opacity: 0 }, gsapDataOptions);
    gsap.from([".progress"], gsapGraphOptions);
}

/**
 * 
 * @param {Boolean} loading 
 */
export const LoadingPage = async (loading) => {

    if (loading) {
        document.querySelector('.loading-page').style.opacity = 1;
        document.querySelector('.loading-page').style.display = 'block';
    } else {

        gsap.fromTo(".loading-page", { opacity: 1 }, { 
            opacity: 0,
            display: 'none',
            duration: 2,
            ease: "circ.out",
            delay: 2,
        });
    }
}

export const DisplayLines = async () => {

    var lines = [];
    
    var line = new LeaderLine(
        document.querySelector('#l1'), 
        /*
        LeaderLine.mouseHoverAnchor(
            document.querySelector('#g1-l1'), 
            'draw', 
            { 
                style: { backgroundImage: null, padding: 0, backgroundColor: "#3498db" }, 
                hoverStyle: { backgroundColor: "#3498db" },
            }
        ), 
        */
        document.querySelector('#l5'), 
        gsapLineOptions
    );

    lines.push(line);

    line = new LeaderLine(
        document.querySelector('#l2'), 
        document.querySelector('#l5'), 
        gsapLineOptions
    );

    lines.push(line);

    lines.forEach(line => {
        line.show();
    });

    // TODO forech all progress
    document.querySelector('body').addEventListener('mousemove', e => {

        lines.forEach(line => {
            line.position();
        });
    });
}

/**
 * 
 * @param {String} value
 */
export const EnabledBoostrapTools = value => {
    $('[data-bs-toggle="tooltip"]').tooltip(value);
    $('[data-bs-toggle="popover"]').popover(value);
}

export const AddZeroToOneNumber = number => (number.toString().length == 1 ? `0${number}` : number);

/**
 * 
 * @param {Number} ms 
 * @returns 
 */
export const Wait = ms => new Promise(res => setTimeout(res, ms));

/**
 * 
 * @param {Boolean} force
 * @param {Boolean} backup
 */
export const SaveData = async (force, backup) => {

    if (savePromise != null) {
        savePromise = null;
    }

    FixGanttFormat();
    ganttToSend = GanttData;

    // TODO if error to send saved => save in local storage, clear it if send is OK !!!!

    if (!backup) {
        SaveHistoric();
    }

    document.querySelector(".save-data").style.opacity = 0;
    document.querySelector(".save-data").innerHTML = `<i class='bi bi-hdd-fill'></i> ${config.language[config.format.language_code].ui.gantt.save_message}`;
    gsap.fromTo(".save-data", { opacity: 1 }, { duration: 1, delay: 3, opacity: 0, ease: "none" });
    
    savePromise = Wait(!force ? config.details.ms_before_to_save : 0);

    await savePromise.finally(() => {
        if (savePromise != null) {
            console.log(`%c${config.language[config.format.language_code].ui.gantt.save_message}`, 'color:#3498db;font-weight:900;', ganttToSend.version, ganttToSend);
            savePromise = null;
        }
    });
}

export const ScrollTo = id => document.querySelector(id).scrollIntoView({ behavior: "instant", block: 'center', inline: "center" });

/**
 * 
 * @param {String} string
 * @param {String} replace
 */
export const StyleSizeToNumber = (string, replace) => Number(string.toString().trim().replace(replace, ""));

export const CheckPicture = picture => picture == "" || picture === undefined ? "lthdg_logo.png" : picture;

export const SaveHistoric = () => {

    const lastHistoric = new GanttClass.Gantt().ToObject(historic.Last());

    if (lastHistoric === undefined || !_.isEqual(lastHistoric.data, GanttData.data)) {

        if (indexHistoric != (historic.length - 1) && historic.length > 0) historic.splice(indexHistoric + 1);

        GanttData.version = index;
        index++;

        historic.push(JSON.parse(JSON.stringify(GanttData)));
    
        if (historic.length >= config.details.limit_historic) historic.shift();

        indexHistoric = (historic.length - 1);

        if (lastHistoric !== undefined) EditHistoric(0);
        else {
            SetDisabled(document.querySelector('.undo-action'), false);
            SetDisabled(document.querySelector('.redo-action'), false);
        }
    }
}

export const EditHistoric = index => {

    indexHistoric += index;

    if (indexHistoric < 0) {
        indexHistoric = 0;
    } else if (indexHistoric >= historic.length) {
        indexHistoric = (historic.length - 1);
    }
    
    SetDisabled(document.querySelector('.undo-action'), (indexHistoric != 0));
    SetDisabled(document.querySelector('.redo-action'), (indexHistoric != (historic.length - 1)));
    
    SetGanttData(historic[indexHistoric]);
    FixGanttFormat();
}

export const CheckIsDisabled = elmnt => elmnt.classList.contains("disabled");

export const SetDisabled = (elmnt, enabled) => {
    
    if (enabled) {
        elmnt.classList.remove("disabled");
    } else {
        elmnt.classList.add("disabled");
    }

    return CheckIsDisabled(elmnt);
}

export const TradList = (list, title)  => {

    config.translate[list].forEach(e => {
        document.querySelectorAll(`[data-traduction="trad-${e}"]`).forEach(elmnt => {

            if (title) {
                elmnt.setAttribute('data-bs-original-title', config.language[config.format.language_code].ui[list][e]);
            } else {
                elmnt.innerHTML = config.language[config.format.language_code].ui[list][e];
            }
        });
    });
}

export const Download = (data, fileName, type) => {

    const elmnt = document.createElement("a");
    const file = new Blob([data], { type: type });
    elmnt.href = URL.createObjectURL(file);
    elmnt.download = fileName;
    elmnt.click();
}

export const OpenFileDialog = callback => {

    let input = document.createElement('input');
    input.type = 'file';

    input.onchange = _this => {
        // let files = Array.from(input.files);
        let file = input.files[0];
        
        if (file.name.match(/\.(jsonp|json)$/)) {
            var reader = new FileReader();

            if (callback && typeof callback == "function") {
                reader.onload = function() {
                    const result = JSON.parse(reader.result);
                    index = 0;
                    result.version = index;
                    historic.Clear();
                    SetGanttData(result);
                    SaveHistoric();
                    callback(false);
                };
            }

            reader.readAsText(file);
        } else {
            document.title = `${config.details.title} · Warning...`;
            modal.OpenMessageModal("#WarningModal", `<i class='bi bi-exclamation-triangle-fill'></i> Warning: You can only select one json file ! (<a href='.'>refresh</a>)`);
        }
    };

    input.click();
}

/**
 * Fix parsing date of start\end (ISO format to Date Object)
 * 
 * @param {Boolean} force (default: false)
 */
export const FixGanttFormat = (force = false) => {

    GanttData = JSON.parse(JSON.stringify(GanttData));
    
    if (force) GanttData = JSON.parse(GanttData);

    GanttData.start = new Date(GanttData.start);
    GanttData.end = new Date(GanttData.end);
}

/**
 * 
 * @param {Boolean} editDateMode (default: false)
 * @param {GanttClass.Task[]} tasks (default: null)
 * @returns {Date[]}
 */
export const GetMinMaxDate = (editDateMode = false, tasks = null) => {
    
    let dateOfStart = null;
    let dateOfEnd = null;
    
    (tasks != null ? tasks : GanttData.data.tasks).forEach(task => {
        if (dateOfStart == null || new Date(task.start).setHours(0, 0, 0, 0) < dateOfStart) {
            dateOfStart =  new Date(task.start);
        }

        if (dateOfEnd == null || new Date(task.end).setHours(0, 0, 0, 0) > dateOfEnd.setHours(0, 0, 0, 0)) {
            dateOfEnd =  new Date(task.end);
        }
    });

    if (editDateMode) {
        dateOfStart.MakeDayOnStartWeek(date.TypeEditModeDate.REMOVE, config.details.number_of_day_before_start);
        dateOfEnd.MakeDayOnStartWeek(date.TypeEditModeDate.ADD, config.details.number_of_day_after_end);
    }

    return [dateOfStart, dateOfEnd];
}

export const SelectItemIndex = () => {
    
    const tickets = GanttData.data.tickets.filter(t => GanttData.data.tasks.filter(f => f.ticket == t.id).length > 0 && !t.isDelete);
    const tasks = GanttData.data.tasks.filter(t => !t.isDelete && t.ticket == (config.details.select_first_task_on_load ? tickets.sort(t => t.order).First() : tickets.sort(t => t.order).Last()).id).sort(t => t.order);

    try {
        return (config.details.select_first_task_on_load ? tasks.First().id : tasks.Last().id);
    } catch (_) {
        return null;
    }
}

/**
 * 
 * @param {String} render 
 * @returns {Node} 
 */
export const WriteNode = render => new DOMParser().parseFromString(render, "text/html").body.firstChild;

/**
 * 
 * @param {Node} node 
 * @returns {String}
 */
export const WriteNodeToString = node => {
    
    if (node instanceof Node) return node.outerHTML;
    else return WriteNode(node).outerHTML;
}

export const AddNewChecklistElement = _ => document.querySelector('#list-checklists').insertAdjacentHTML(InsertCodePosition.BEFOREEND, WriteNodeToString(view.RenderChecklistElement({ id: GenerateId() })));

/**
 * 
 * @param {GanttClass.Task} task 
 * @returns {GanttClass.Ticket}
 */
export const GetTicketOfTask = task => GanttData.data.tickets.find(i => i.id == task.ticket);

/**
 * 
 * @param {String} id 
 * @returns {GanttClass.Ticket}
 */
export const GetTicketWithId = id => GanttData.data.tickets.find(i => i.id == id);

/**
 * 
 * @param {String} id 
 * @returns {GanttClass.Task}
 */
export const GetTaskWithId = id => GanttData.data.tasks.find(i => i.id == id);

export const DeleteTask = (task) => {
    // task delete value to true
}
