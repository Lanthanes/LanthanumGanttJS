"use strict";

String.prototype.DatePadStartZero = function() {

    let result = "";

    if (this.split('/').length > 1) {

        this.split('/').forEach((e, i, a) => {
            result += e.padStart(2, '0') + ((i !== a.length - 1) ? "/" : "");
        });
    } else {
    
        this.split('-').forEach((e, i, a) => {
            result += e.padStart(2, '0') + ((i !== a.length - 1) ? "-" : "");
        });
    }

    return result;
};
