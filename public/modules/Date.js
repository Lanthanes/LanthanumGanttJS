"use strict";

import { main as config } from '../config.js';

/**
 * 
 * @param {Date} start 
 * @param {Date} end 
 * @returns difference of day between two date
 */
export const DiffDay = (start, end) => Math.ceil(Math.abs((start - end) / 86400000)) + 1; // 86400000 => day in timestamp => 24 * 60 * 60 * 1000

/**
 * 
 * @param {Date} start 
 * @param {Date} end 
 * @returns difference of week between two date
 */
export const DiffWeek = (start, end) => Math.round((end - start) / (7 * 24 * 60 * 60 * 1000)); // 86400000 => day in timestamp

/**
 * 
 * @param {TypeEditModeDate} mode
 * @param {Number} add
 * @returns 
 */
Date.prototype.MakeDayOnStartWeek = function(mode, add) {
    
    switch (mode) {
        case TypeEditModeDate.ADD:
            this.setDate(this.getDate() + add);
            break;
        case TypeEditModeDate.REMOVE:
            this.setDate(this.getDate() - add);
            break;
    }

    if (this.getDay() != 1) {
        this.setDate((this.getDate() - this.getDay()) + 1);
    }

    return this;
};

Date.prototype.GetWeekOnYear = function() {

    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));

    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));

    return Math.ceil((((d - yearStart) / 86400000) + 1)/7);
};

/**
 * 
 * @param {number} day
 * @returns 
 */
Date.prototype.EditDay = function(day) {
    this.setDate(this.getDate() + day);
    return this;
}

Date.prototype.FormatDateToString = function() {
    return this.toLocaleString(config.format.timezone, config.format.date_option).slice(0, 10).replace(',', '').DatePadStartZero().trim();
}

export const TypeEditModeDate = {
    ADD:        0,
    REMOVE:     1
};
