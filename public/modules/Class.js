"use strict";

export const Gantt = class Gantt {
    
    version = 0;

    start;
    end;
    data;

    /**
     * 
     * @param {Date} start 
     * @param {Date} end 
     * @param {Data} data 
     */
    constructor(start, end, data) {
        this.start = start;
        this.end = end;
        this.data = data;

        // this.start.setHours(0, 0, 0, 0);
        // this.end.setHours(0, 0, 0, 0);
    }

    /**
     * 
     * @param {*} obj 
     * @returns {Gantt}
     */
    ToObject(obj) {
        return obj && Object.assign(this, obj);
    }
}

export const Data = class Data {

    tickets = [new Ticket()]
    categories = [new Item()]
    roles = [new Item()]
    profiles = [new Profile()]
    tasks = [new Task()]
}

export const Item = class Item {

    id = 0;
    label = `#${this.id} · no label`;
    description = "";
    order = 0;                              // order on display

    isDelete = false;
    isNew = false;
    isEdit = false;
}

export const Task = class Task extends Item {

    ticket;
    profile = new Profile();
    role = new Item();                      // role for ticket
    start = new Date();                     // task start date
    end = new Date();                       // task end date
    finished = 0;                           // 0 = no finished (0%) to 100 = is finished (100%)
    dependencies = [new Number()];          // dependencies of others tasks (list of id),
    checklists = [new Checklist()];
}

export const Checklist = class Checklist extends Item {

    isFinished = false;
}

export const Profile = class Profile extends Item {

    picture = "";
}

export const Ticket = class Ticket extends Profile {
    
    category;       // category of ticket
    link;           // link of ticket for example
}
