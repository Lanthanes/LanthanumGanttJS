export const main = {
    url : {
        get: "./demo/data.json",
        post: ""
    },
    format: {
        timezone: "fr-FR",
        language_code: "fr",
        date_option: "{ dateStyle: 'short' }",
    },
    translate: {
        menu: ["action_button", "dropdown_ticket_button", "dropdown_profiles_button", "dropdown_roles_button", "dropdown_categories_button", "dropdown_clear_button"],
        title: ["today_button", "undo_button", "redo_button", "export_button", "import_button", "send_button", "refresh_button"],
        gantt: [],
    },
    language : {
        fr : {
            name: "français",
            ui: {
                menu: {
                    action_button: "Edition",
                    delete_button: "Supprimer",
                    dropdown_manage_button: "Gestion",
                    dropdown_ticket_button: "Ajouter une Tâche",
                    dropdown_profiles_button: "Gérer les Profiles",
                    dropdown_roles_button: "Gérer les Roles",
                    dropdown_categories_button: "Gérer les Catégories",
                    dropdown_clear_button: "Effacer toutes les données",
                    how_to_delete_checklist_element: "Pour supprimer un élément de la checklist, veuillez simplement vider le champ texte !",
                },
                title: {
                    today_button: "Aujourd'hui",
                    undo_button: "Annuler",
                    redo_button: "Rétablir",
                    export_button: "Exporter",
                    import_button: "Importer",
                    send_button: "Sauvegarder",
                    refresh_button: "Actualiser",
                },
                gantt: {
                    loading_page: "Chargement",
                    day_info: "jour(s)",
                    week_info: "Semaine",
                    week_code: ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
                    no_dependance: "Aucune dépandance",
                    enter_text: "Ecrire une valeur...",
                    save_message: "Données enregistrées...",
                }
            }
        },
        en : {
            name: "english",
            ui: {
                menu: {
                    action_button: "Edition",
                    delete_button: "Delete",
                    dropdown_manage_button: "Manage",
                    dropdown_ticket_button: "Add Task",
                    dropdown_profiles_button: "Manage Profiles",
                    dropdown_roles_button: "Manage Roles",
                    dropdown_categories_button: "Manage Categories",
                    dropdown_clear_button: "Clear All Data",
                    how_to_delete_checklist_element: "To remove item from the checklist, just empty the text field !",
                },
                title: {
                    today_button: "Today",
                    undo_button: "Undo",
                    redo_button: "Redo",
                    export_button: "Export",
                    import_button: "Import",
                    send_button: "Save",
                    refresh_button: "Refresh"
                },
                gantt: {
                    loading_page: "Loading",
                    day_info: "day(s)",
                    week_info: "Week",
                    week_code: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                    no_dependance: "no dependency",
                    enter_text: "Enter a value...",
                    save_message: "Data saved...",
                }
            }
        }
    },
    details : {
        day_size_pixel: 70,
        limit_historic: 10,
        title : "Lanthanum Gantt JS",
        number_of_day_before_start: 28,
        number_of_day_after_end: 28,
        display_pourcent_on_progress_bar: false,
        zuto_save: false,
        ms_before_to_save: 3000,
        select_first_task_on_load: false,
    }
}
