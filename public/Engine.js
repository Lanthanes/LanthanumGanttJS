/*! LanthanumGanttJS v1.0 lanthanes https://lanthanes.gitlab.io/LanthanumGanttJS/ */
"use strict";

import * as GanttClass from './modules/Class.js';
import * as date from './modules/Date.js';
import * as string from './modules/String.js';
import * as utils from './modules/Utils.js';
import * as edit from './modules/Edit.js';
import * as view from './modules/View.js';
import * as modal from './modules/Modal.js';
import { yesNoCallback } from './plugins/yesnomodal.js'
import { main as config } from './config.js';

const diagramm_background = document.querySelector('.diagramm-background');
const diagramm_calendar = document.querySelector('.diagramm-calendar');
const diagramm_profile = document.querySelector('.diagramm-profile');
const diagramm_graph = document.querySelector('.diagramm-content');

const dragContent = document.querySelector('.dragscroll');

let mouseonover = false;
let mouseX = null;
let taskSelectionValue = new Map();
let taskSelectionType = null;
/** @type {Date} */
let dateOfStart = null, dateOfEnd = null;
let lastTaskEdit = null;

// TODO create view file for app (like GUILDBOARD tool)

/**
 * 
 * @param {Boolean} isFirstlaunch 
 * @returns {Boolean}
 */
const LanthGantt = async (isFirstlaunch) => {

    try {
        if (typeof config === 'undefined' || _.isEmpty(config)) {
            utils.LoadingPage(true);
            document.title = `Error Error Error...`;
    
            modal.OpenMessageModal("#ErrorModal", "<i class='bi bi-file-earmark-excel-fill'></i> Config file is not defined (<a href='.'>refresh</a>)");
    
            return;
        }

        utils.EnabledBoostrapTools('disable');

        document.title = `${config.details.title} · ${config.language[config.format.language_code].ui.gantt.loading_page}...`;
        document.querySelector(':root').style.setProperty('--day-size-pixel', `${config.details.day_size_pixel}px`);

        utils.SetDisabled(document.querySelector('.on-today'), false);
    
        let getStartDate = document.querySelector('#calendar-date-start').value.toLocaleString(config.format.timezone, config.format.date_option);
        let getEndDate = document.querySelector('#calendar-date-end').value.toLocaleString(config.format.timezone, config.format.date_option);
    
        if (isFirstlaunch) {
    
            let params = (new URL(window.location.href)).searchParams;
    
            if (params.get('start') == null || params.get('end') == null) {
                const startDate = new Date(), endDate = new Date(); // Date.now()
                endDate.setMonth(startDate.getMonth() + 1);
    
                getStartDate = startDate.toISOString().split('T')[0]; // .slice(0,10)
                getEndDate = endDate.toISOString().split('T')[0]; // .slice(0,10)
            } else {
                getStartDate = params.get('start').toLocaleString(config.format.timezone, config.format.date_option);
                getEndDate = params.get('end').toLocaleString(config.format.timezone, config.format.date_option);
            }
    
            document.querySelector('#calendar-date-start').value = getStartDate;
            document.querySelector('#calendar-date-end').value = getEndDate;
        
            utils.LoadingPage(true);
        }
    
        const request = utils.CallAjaxURL(config.url.get, 'jsonp', 'GET', (`start=${new Date(getStartDate).FormatDateToString()}&end=${new Date(getEndDate).FormatDateToString()}`));
    
        if (request.readyState) {
        
            request.done((m) => {
        
                if (utils.GanttData == null){
                    utils.SetGanttData(new GanttClass.Gantt(new Date(getStartDate), new Date(getEndDate), m));
                    utils.SaveHistoric();
                }
                
                if (utils.GanttData != null && utils.GanttData.start instanceof Date && utils.GanttData.end instanceof Date && utils.GanttData.start < utils.GanttData.end && utils.GanttData.data.tickets.length > 0 && utils.GanttData.data.tasks.length > 0) {
    
                    diagramm_background.innerHTML = "";
                    diagramm_calendar.innerHTML = "";
                    diagramm_profile.innerHTML = "";
                    diagramm_graph .innerHTML = "";

                    const minMax = utils.GetMinMaxDate(true);
    
                    dateOfStart = minMax[0];
                    dateOfEnd = minMax[1];
            
                    let enumDay = new Date(dateOfStart), enumDayForNext = new Date(dateOfStart);
            
                    for (let w = 0; w < date.DiffWeek(dateOfStart, dateOfEnd); w++) {
        
                        // TODO change to function for scroll event
                        enumDayForNext.setDate(enumDay.getDate() + 6);

                        let dayInWeekData = "";
                        let dayInWeekBackground = "";

                        for (let d = 0; d < 7; d++) {

                            dayInWeekData += utils.WriteNodeToString(view.RenderDayInWeekData({
                                day: enumDay,
                                dayOnWeek: d,
                            }));

                            dayInWeekBackground += utils.WriteNodeToString(view.RenderDayInWeekBackground({
                                day: enumDay,
                                dayOnWeek: d,
                            }));
        
                            if (enumDay.setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0)) utils.SetDisabled(document.querySelector('.on-today'), true);
            
                            enumDay.EditDay(1);
                        }

                        diagramm_calendar.appendChild(view.RenderWeekData({
                            listOfDay: dayInWeekData,
                            day: enumDay,
                            nextDay: enumDayForNext,
                        }));

                        diagramm_background.appendChild(view.RenderWeekBackground({
                            listOfDay: dayInWeekBackground,
                            status: (w == (date.DiffWeek(dateOfStart, dateOfEnd) - 1) ? 'end-calandar' : (w == 0 ? 'start-calandar' : '')),
                        }));
                    }
    
                    if (utils.GanttData.data.tasks.length > 0) {
    
                        utils.GanttData.data.tickets.sort(t => t.order).forEach(ticket => {
                            if (utils.GanttData.data.tasks.filter(i => i.ticket == ticket.id).filter(t => !t.isDelete).length > 0) 
                                edit.WriteTicketNode(ticket, dateOfStart, dateOfEnd, false, [diagramm_profile, diagramm_graph]);
                        });

                        let index = utils.SelectItemIndex();
    
                        utils.EnabledBoostrapTools('enable');
                        utils.EnabledBoostrapTools('hide');
    
                        if (index != null) {
    
                            // utils.DisplayLines();
                            if (isFirstlaunch) {
                                utils.AnimateOnLoad();
                                utils.ScrollTo(`#tag${index}`);
                            } else if (lastTaskEdit != null) {
                                utils.ScrollTo(`#tag${lastTaskEdit}`);
                                lastTaskEdit = null;
                            }
                        }
                        
                        InitEventGraph();
                    }
    
                    utils.TradList('menu', false);
                    utils.TradList('title', true);
    
                    document.title = config.details.title;
                } else {
                    document.title = `${config.details.title} · Warning...`;
                    modal.OpenMessageModal("#WarningModal", `<i class='bi bi-exclamation-triangle-fill'></i> Warning: Check if date values conform, if date start is less than date end or if data received is not empty ! (<a href='.'>refresh</a>)`);
                }
        
                utils.LoadingPage(false);

                return true;
            });
            
            request.fail((_, status) => {
                utils.LoadingPage(true);
                document.title = `${config.details.title} · Error...`;
    
                modal.OpenMessageModal("#ErrorModal", `<i class="bi bi-exclamation-octagon-fill"></i> URL connection: ${status} (<a href='.'>refresh</a>)`);

                return false;
            });
        } else {
            utils.LoadingPage(true);
            document.title = `${config.details.title} · Error...`;
    
            modal.OpenMessageModal("#ErrorModal", "<i class='bi bi-ethernet'></i> URL connection: error (<a href='.'>refresh</a>)");

            return false;
        }        
    } catch (error) {
            utils.LoadingPage(true);
            document.title = `OOPS...`;
    
            modal.OpenMessageModal("#ErrorModal", `<i class='bi bi-bug-fill'></i> WOAH (<a href='.'>refresh</a>) => ${error}`);

            return false;
    }
};

document.querySelector('#calendar-valid').addEventListener("click", _ => LanthGantt(false));

document.querySelector('#refresh-page').addEventListener("click", _ => window.location.reload());

dragContent.addEventListener("scroll", _ => {
    
    if (dragContent.scrollLeft >= (diagramm_background.offsetWidth - dragContent.offsetWidth)) {
        // TODO add week to right
    } else if (dragContent.scrollLeft == 0) {
        // TODO add week to left
    }

    document.querySelector('.diagramm-profile').scrollTop = dragContent.scrollTop;
});

document.querySelector('.diagramm-profile').addEventListener("scroll", _ => dragContent.scrollTop = document.querySelector('.diagramm-profile').scrollTop);

document.querySelector("#form-task-submit").addEventListener("click", _ => {

    const taskIndex = utils.GanttData.data.tasks.findIndex(t => t.id == [document.forms["TaskForm"]["TaskID"].value] && t.ticket == [document.forms["TaskForm"]["TicketID"].value]);

    let task = new GanttClass.Task();

    if (taskIndex  != -1) {
        task = utils.GanttData.data.tasks[taskIndex];
    }

    task.ticket = document.forms["TaskForm"]["tickets"].value;

    task.label = document.forms["TaskForm"]["label"].value;
    task.description = document.forms["TaskForm"]["description"].value;

    task.start = document.forms["TaskForm"]["task-date-start"].value;
    task.end = document.forms["TaskForm"]["task-date-end"].value;

    task.profile = document.forms["TaskForm"]["users"].value;
    task.role = document.forms["TaskForm"]["roles"].value;
    task.finished = document.forms["TaskForm"]["finished"].value;

    document.querySelectorAll('#list-checklists .content-checklist').forEach(e => {
        let checklist = task.checklists.find(c => c.id == e.querySelector('input[name="checklist-label"]').id);

        if (checklist == undefined) {
            checklist = new GanttClass.Checklist();
            checklist.id = e.querySelector('input[name="checklist-label"]').id;
        } else {
            checklist.isEdit = true;
        }

        if (e.querySelector('input[name="checklist-label"]').value.toString().trim() == "") {
            checklist.isDelete = true;
        } else {
            checklist.label = e.querySelector('input[name="checklist-label"]').value;
        }

        checklist.isFinished = e.querySelector('input[name="checklist-check"]').checked;

        console.log("checklist", checklist);
        if (!checklist.isEdit && !checklist.isDelete) {
            task.checklists.push(checklist);
        }
    });


    document.querySelectorAll('#list-dependencies .content-dependency').forEach(e => {
        if (e.querySelector('input[name="dependency-check"]').checked && task.dependencies.find(d => d == e.querySelector('input[name="dependency-check"]').id) == undefined) {
            task.dependencies.push(e.querySelector('input[name="dependency-check"]').id);
        }
    });

    // TODO check label empty ???
    if (new Date(task.start).setHours(0, 0, 0, 0) > new Date(task.end).setHours(0, 0, 0, 0)) {
        document.title = `${config.details.title} · Warning...`;
        modal.OpenMessageModal("#WarningModal", `<i class='bi bi-exclamation-triangle-fill'></i> Warning: Check if date values conform, if date start is less than date end !`);
        return;
    }

    const ticket = utils.GetTicketWithId(task.ticket);

    if (taskIndex != -1) {
        
        task.isEdit = true;
        utils.GanttData.data.tasks[taskIndex] = task;
        
        edit.WriteTicketNode(ticket, dateOfStart, dateOfEnd, true);
    } else {
        const arr = utils.GanttData.data.tasks.filter(t => t.ticket == task.ticket);
        task.isNew = true;
        task.id = utils.GenerateId();
        task.order = arr.length + 1;
        utils.GanttData.data.tasks.push(task);

        arr.sort(t => t.order);

        if (task.order > 1) {
            edit.WriteTaskNode(task, dateOfStart, dateOfEnd, true, [document.querySelector(`#tas${arr.Last().id}`).parentNode, document.querySelector(`#tag${arr.Last().id}`).parentNode]);
        } else {
            edit.WriteTicketNode(ticket, dateOfStart, dateOfEnd, false, [diagramm_profile, diagramm_graph]);
            edit.WriteTaskNode(task, dateOfStart, dateOfEnd, true, [document.querySelector(`#tic${ticket.id}`).parentNode, document.querySelector(`#tig${ticket.id}`).parentNode]);
        }

        InitEventGraph();
    }
    
    $('#TaskModal').modal('hide');
    
    utils.SaveData(false, false);

    const minMax = utils.GetMinMaxDate(true);

    if (minMax[0] != dateOfStart || minMax[1] != dateOfEnd) {
        LanthGantt(false);
        lastTaskEdit = task.id;
    } else {
        utils.ScrollTo(`#tag${task.id}`);
    }
            
    utils.EnabledBoostrapTools('enable');
    utils.EnabledBoostrapTools('hide');
});

document.querySelector(".open-new-task").addEventListener("click", e => modal.OpenModalTask(null, null));

document.querySelector(".redo-action").addEventListener("click", _ => {
    utils.EditHistoric(+1);
    CallBackSelection(false);
});
document.querySelector(".undo-action").addEventListener("click", _ => {
    utils.EditHistoric(-1);
    CallBackSelection(false);
});

document.querySelector('#new-item-checklist').addEventListener('click', _ => utils.AddNewChecklistElement());

document.querySelector(".export-data").addEventListener("click", _ => utils.Download([JSON.stringify(utils.GanttData)], "test.json", "application/json"));
document.querySelector(".import-data").addEventListener("click", _ => utils.OpenFileDialog(CallBackSelection));

document.querySelector(".send-data").addEventListener("click", _ => utils.SaveData(true, false));

document.querySelector(".on-today").addEventListener("click", _ => utils.ScrollTo('.now'));

document.querySelectorAll(".open-list").forEach(a => {
    a.addEventListener("click", e => {
        modal.OpenModalList(e.currentTarget.getAttribute('data-list-item'));
    });
});

document.querySelector("#yesNo-CallYes").addEventListener("click", _ => {
    yesNoCallback[0].call();
});

document.querySelector("#yesNo-CallNo").addEventListener("click", _ => {
    yesNoCallback[1].call();
});

document.querySelector("#modal-delete-task").addEventListener("click", e => {
    e.currentTarget.getAttribute('data-id-delete'); // check if == '-1' => modal to set it delete
    // yesNoCallback = modal.OpenYesNoModal(modal.ModalType.YesNo, "test", utils.DeleteTask(), null);
});

document.querySelector(".task-range").addEventListener("input", e => document.querySelector(".task-range-value .tooltip-inner").innerHTML = `${e.target.value}%`);
document.querySelector(".task-range").addEventListener("mousemove", e => document.querySelector(".task-range-value .tooltip-inner").innerHTML = `${e.target.value}%`);

document.addEventListener("mouseup", a => {

    if (a.button == 0) {

        utils.EnabledBoostrapTools('hide');
        utils.EnabledBoostrapTools('disable');

        let mouseMove = mouseX - a.pageX;

        if (mouseX != null && taskSelectionType != null && mouseMove != 0) {

            switch (taskSelectionType) {
                case utils.SelectionType.PLACE:
                    taskSelectionValue.forEach((e, k) => {

                        const elmnt = document.querySelector(`#${k}`);

                        if (elmnt != null && !elmnt.classList.contains('task-group')) {

                            let newValue = e.margin + ((Math.ceil((mouseMove) / config.details.day_size_pixel) * config.details.day_size_pixel) * (-1));                            
                            elmnt.style.marginLeft = `${newValue}px`;

                            edit.EditMoveGanttBar(elmnt.getAttribute('data-taskid'), elmnt.getAttribute('data-ticketid'), elmnt, dateOfStart, dateOfEnd);

                            lastTaskEdit = elmnt.getAttribute('data-taskid');
                        }
                    });
                    
                    break;
                case utils.SelectionType.SIZE:

                    const task = taskSelectionValue.entries().next().value;
                    const elmnt = document.querySelector(`#${task[0]}`);
                    
                    let newValue = ((Math.ceil((mouseMove) / config.details.day_size_pixel) * config.details.day_size_pixel) * (-1));

                    if (task[1].elmnt.classList.contains('left')) {
                        elmnt.style.marginLeft = `${task[1].margin + newValue}px`
                        elmnt.style.width = `${task[1].width - newValue}px`
                    } else {
                        elmnt.style.width = `${task[1].width + newValue}px`
                    }

                    if (utils.StyleSizeToNumber(elmnt.style.width, "px") < config.details.day_size_pixel) {
                        elmnt.style.width = `${config.details.day_size_pixel}px`
                    }
                    
                    edit.EditMoveGanttBar(elmnt.getAttribute('data-taskid'), elmnt.getAttribute('data-ticketid'), elmnt, dateOfStart, dateOfEnd);

                    lastTaskEdit = elmnt.getAttribute('data-taskid');

                    break;
            }

            utils.SaveData(false, false);

            const minMax = utils.GetMinMaxDate(true);

            if (minMax[0].getTime() != dateOfStart.getTime() || minMax[1].getTime() != dateOfEnd.getTime()) {
                LanthGantt(false);
            }
                            
            InitEventGraph();
        }
        
        utils.EnabledBoostrapTools('enable');
        utils.EnabledBoostrapTools('hide');
        
        mouseX = null;
        mouseonover = false;
        dragscroll.canScroll = true;
        dragscroll.reset();
        taskSelectionValue = new Map();
        taskSelectionType = null;
    }
});

    
document.addEventListener("mousemove", a => {
    
    if (mouseonover) {

        utils.EnabledBoostrapTools('hide');
        utils.EnabledBoostrapTools('disable');

        dragscroll.canScroll = false;
        dragscroll.reset();

        if (mouseX != null && taskSelectionType != null) {

            const direction = (mouseX - a.pageX) * (-1);
            let newValue = (direction / config.details.day_size_pixel) * config.details.day_size_pixel;

            switch (taskSelectionType) {
                case utils.SelectionType.PLACE:
                    taskSelectionValue.forEach((e, k) => document.querySelector(`#${k}`).style.marginLeft = `${e.margin + newValue}px`);
                    break;
                case utils.SelectionType.SIZE:
                    const task = taskSelectionValue.entries().next().value;

                    if (task[1].elmnt.classList.contains('left')) {
                        document.querySelector(`#${task[0]}`).style.marginLeft = `${task[1].margin + newValue}px`
                        document.querySelector(`#${task[0]}`).style.width = `${task[1].width - newValue}px`
                    } else {
                        document.querySelector(`#${task[0]}`).style.width = `${task[1].width + newValue}px`
                    }

                    if (utils.StyleSizeToNumber(document.querySelector(`#${task[0]}`).style.width, "px") < config.details.day_size_pixel) {
                        document.querySelector(`#${task[0]}`).style.width = `${config.details.day_size_pixel}px`
                    }
                    
                    break;
            }
        }
    }
});

$(() => {
    $.contextMenu({
        selector: '.user-content, .selection-task',
        // callback: (key, options) => {},
        items: {
            edit: { 
                name: config.language[config.format.language_code].ui.menu.action_button, 
                icon: "fas fa-edit", 
                callback: (_, o) => {
                    modal.OpenModalTask(o.$trigger[0].getAttribute("data-TicketID"), o.$trigger[0].getAttribute("data-TaskID"), utils.GanttData);
                }
            },
            delete: { 
                name: config.language[config.format.language_code].ui.menu.delete_button, 
                icon: "fas fa-trash-alt", 
                callback: (i, o, e) => {
                    console.log("delete", i, o, e); 
                }
            },
            sep1: "---------",
            add: { 
                name: config.language[config.format.language_code].ui.menu.dropdown_ticket_button, 
                icon: "fas fa-plus-square", 
                callback: () => {
                    modal.OpenModalTask(null, null);
                }
            },
            sep2: "---------",
            manage: {
                name: config.language[config.format.language_code].ui.menu.dropdown_manage_button, 
                items: {
                    profiles: { 
                        name: config.language[config.format.language_code].ui.menu.dropdown_profiles_button, 
                        icon: "fas fa-users", 
                        callback: () => {
                            modal.OpenModalList("profiles");
                        }
                    },
                    roles: { 
                        name: config.language[config.format.language_code].ui.menu.dropdown_roles_button, 
                        icon: "fas fa-bookmark" , 
                        callback: () => {
                            modal.OpenModalList("roles");
                        }
                    },
                    categories: {
                         name: config.language[config.format.language_code].ui.menu.dropdown_categories_button, 
                         icon: "fas fa-tags" , 
                         callback: () => {
                            modal.OpenModalList("categories");
                         }
                    },
                },
                icon: "fas fa-caret-right"
            },
            sep3: "---------",
            clear: { 
                name: config.language[config.format.language_code].ui.menu.dropdown_clear_button, 
                icon: "fas fa-eraser", 
                callback: (i, o, e) => {
                    console.log("clear", i, o, e);
                }
            },
        }
    });
});

const CallBackSelection = (force) => {

    lastTaskEdit = null;
    utils.FixGanttFormat(force);
    utils.SaveData(false, true);
    LanthGantt(false);
}

const MoveGraphBar = (i, m, e, t) => {

    mouseonover = true;
    mouseX = m.pageX;

    if (e.classList.contains('task-group')) {
        document.querySelectorAll(`[data-ticketid="${e.getAttribute("data-TicketID")}"]`).forEach(l => {
            if (l.classList.contains('move-task-graph')) {
                taskSelectionValue.set(l.id, { margin: utils.StyleSizeToNumber(l.style.marginLeft, "px"), width: utils.StyleSizeToNumber(l.style.width, "px"), elmnt: l });
            }
        });
    } else {
        taskSelectionValue.set(i.id, { margin: utils.StyleSizeToNumber(i.style.marginLeft, "px"), width: utils.StyleSizeToNumber(i.style.width, "px"), elmnt: e });
    }

    taskSelectionType = t;
}

// TODO add remove listener action ???
const InitEventGraph = () => {
    
        
    document.querySelectorAll(".user-content").forEach(e => {
        
        e.addEventListener("dblclick", a => {
            
            modal.OpenModalTask(a.currentTarget.getAttribute("data-TicketID"), a.currentTarget.getAttribute("data-TaskID"), utils.GanttData);
        });
    });

    document.querySelectorAll(".go-to-graph").forEach(e => {
    
        e.addEventListener("click", a => {

            if (a.currentTarget.id.length > 0) {
                if (a.currentTarget.id.slice(0, 3) == "tic"){
                    utils.ScrollTo(`#tig${a.currentTarget.id.slice(3, a.currentTarget.id.length)}`);
                } else if (a.currentTarget.id.slice(0, 3) == "tas") {
                    utils.ScrollTo(`#tag${a.currentTarget.id.slice(3, a.currentTarget.id.length)}`);
                }
            }
        });
    });

    document.querySelectorAll(".open-task").forEach(e => {
        
        e.addEventListener("click", a => {
            
            modal.OpenModalTask(a.currentTarget.getAttribute("data-TicketID"), a.currentTarget.getAttribute("data-TaskID"));
        });
    });

    document.querySelectorAll(".selection-task").forEach(e => {

        e.addEventListener("dblclick", a => {
            
            modal.OpenModalTask(a.currentTarget.getAttribute("data-TicketID"), a.currentTarget.getAttribute("data-TaskID"));
        });
    });

    document.querySelectorAll(".move-task-graph").forEach(e => {

        e.addEventListener("mousedown", a => {

            if (a.button == 0) {
                
                mouseonover = true;
                mouseX = a.pageX;

                utils.EnabledBoostrapTools('hide');
                utils.EnabledBoostrapTools('disable');

                if (a.target.classList.contains('progress-edit-bar')) MoveGraphBar(a.currentTarget, a, a.target, utils.SelectionType.SIZE);
                else MoveGraphBar(a.currentTarget, a, a.currentTarget, utils.SelectionType.PLACE);
            }
        });
    });
}

window.addEventListener("load", _ => LanthGantt(true), { once: true });
